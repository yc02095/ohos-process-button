package com.dd.processbutton;

import com.dd.library.ResourceTable;

import com.dd.processbutton.utils.AttrUtils;
import com.dd.processbutton.utils.HmsResourcesManager;
import com.dd.processbutton.utils.Utils;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Button;
import ohos.agp.components.ComponentState;
import ohos.agp.components.element.*;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class FlatButton extends Button {
    private StateElement mNormalDrawable;
    private CharSequence mNormalText;
    private float cornerRadius;

    public FlatButton(Context context) {
        super(context);
        init(context, null);
    }

    public FlatButton(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(context, attrSet);
    }

    public FlatButton(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context, attrSet);
    }

    private void init(Context context, AttrSet attrs) {
        mNormalDrawable = new StateElement();
        if (attrs != null) {
            initAttributes(context, attrs);
        }
        mNormalText = getText();
        setBackgroundCompat(mNormalDrawable);
    }

    private void initAttributes(Context context, AttrSet attr) {
        if (attr == null) {
            return;
        }

        float defValue = getDimension(ResourceTable.Float_corner_radius);

        cornerRadius = AttrUtils.getFloatValueByAttr(attr, "cornerRadius", defValue);

        mNormalDrawable.addState(new int[]{ComponentState.COMPONENT_STATE_PRESSED},
                createPressedDrawable(attr));
        mNormalDrawable.addState(new int[]{ComponentState.COMPONENT_STATE_FOCUSED},
                createPressedDrawable(attr));
        mNormalDrawable.addState(new int[]{ComponentState.COMPONENT_STATE_SELECTED},
                createPressedDrawable(attr));
        mNormalDrawable.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, createNormalDrawable(attr));
    }

    private ShapeElement createNormalDrawable(AttrSet attr) {
        ElementContainer elementContainer = new ElementContainer();
        ElementContainer.ElementState elementState = new ElementContainer.ElementState();
        ShapeElement drawableTop = new ShapeElement();
        drawableTop.setShape(ShapeElement.RECTANGLE);
        drawableTop.setCornerRadius(
                HmsResourcesManager.getFloatValueByResources(getContext(),ResourceTable.Float_corner_radius));
        drawableTop.setRgbColor(RgbColor.fromArgbInt(
                HmsResourcesManager.getColorValueByResources(getContext(),ResourceTable.Color_blue_pressed)));
        drawableTop.setRgbColor(RgbColor.fromArgbInt(
                HmsResourcesManager.getColorValueByResources(getContext(),ResourceTable.Color_blue_normal)));
        int blueDark = getColor(ResourceTable.Color_blue_pressed);
        int blueDark1 = getColor(ResourceTable.Color_blue_normal);
        int colorPressed = AttrUtils.getColorValueByAttr(attr,"pb_colorPressed", new Color(blueDark)).getValue();
        int colorNormal = AttrUtils.getColorValueByAttr(attr,"pb_colorNormal", new Color(blueDark1)).getValue();
        drawableTop.setRgbColor(RgbColor.fromArgbInt(colorPressed));
        drawableTop.setRgbColor(RgbColor.fromArgbInt(colorNormal));
        elementState.addChildElement(drawableTop);
        ShapeElement drawableElement = new ShapeElement();
        elementState.addChildElement(drawableElement);
        elementContainer.setElementState(elementState);
        return drawableTop;
    }

    private ShapeElement createPressedDrawable(AttrSet attr) {
        ShapeElement drawablePressed = new ShapeElement();
        drawablePressed.setCornerRadius(HmsResourcesManager.getFloatValueByResources(getContext(),ResourceTable.Float_corner_radius));
        drawablePressed.setRgbColor(RgbColor.fromArgbInt(HmsResourcesManager.getColorValueByResources(getContext(),ResourceTable.Color_blue_pressed)));
        drawablePressed.setCornerRadius(getCornerRadius());

        int blueDark = getColor(ResourceTable.Color_blue_pressed);
        int colorPressed = AttrUtils.getColorValueByAttr(attr,"pb_colorPressed",new Color(blueDark)).getValue();
        drawablePressed.setRgbColor(RgbColor.fromArgbInt(colorPressed));

        return drawablePressed;
    }

    protected PixelMapElement getDrawable(int id) {
        return Utils.getPixelMapElement(getContext(),id);
    }

    protected float getDimension(int id) {
        return HmsResourcesManager.getFloatValueByResources(getContext(),id);
    }

    protected int getColor(int id) {
        return HmsResourcesManager.getColorValueByResources(getContext(), id);
    }

    public float getCornerRadius() {
        return cornerRadius;
    }

    public StateElement getNormalDrawable() {
        return mNormalDrawable;
    }

    public String getNormalText() {
        return mNormalText.toString();
    }

    public void setNormalText(CharSequence normalText) {
        mNormalText = normalText;
    }

    /**
     * Set the View's background. Masks the API changes made in Jelly Bean.
     *
     * @param drawable
     */
    public void setBackgroundCompat(Element drawable) {
        int pL = getPaddingLeft();
        int pT = getPaddingTop();
        int pR = getPaddingRight();
        int pB = getPaddingBottom();

        setBackground(drawable);

        setPadding(pL, pT, pR, pB);
    }
}
