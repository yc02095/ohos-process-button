package com.dd.processbutton;

import com.dd.library.ResourceTable;
import com.dd.processbutton.utils.AttrUtils;
import com.dd.processbutton.utils.HmsResourcesManager;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;

public abstract class ProcessButton extends FlatButton implements Component.DrawTask {
    private static final int DEFAULT_MAX_VALUE = 100;

    private int mProgress;
    private int mMaxProgress;
    private int mMinProgress;

    private ShapeElement mProgressDrawable;
    private Paint mProgressPaint;
    private ShapeElement mCompleteDrawable;
    private ShapeElement mErrorDrawable;

    private CharSequence mLoadingText;
    private CharSequence mCompleteText;
    private CharSequence mErrorText;

    public ProcessButton(Context context) {
        super(context);
        init(context, null);
    }

    public ProcessButton(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(context, attrSet);
    }

    public ProcessButton(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context, attrSet);
    }

    @Override
    public void setTextSize(int size) {
        super.setTextSize(size);
    }

    private void init(Context context, AttrSet attrs) {
        mMinProgress = 0;
        mMaxProgress = DEFAULT_MAX_VALUE;
        addDrawTask(this, DrawTask.BETWEEN_BACKGROUND_AND_CONTENT);
        mProgressDrawable = getRectProgressShapeElement();
        mProgressDrawable.setCornerRadius(getCornerRadius());

        mProgressPaint = getRectProgressPaint();

        mCompleteDrawable = getRectCompleteShapeElement();
        mCompleteDrawable.setCornerRadius(getCornerRadius());

        mErrorDrawable = getRectErrorShapeElement();
        mErrorDrawable.setCornerRadius(getCornerRadius());

        setTextSize(60);
        if (attrs != null) {
            initAttributes(context, attrs);
        }
    }

    protected Paint getRectProgressPaint() {
        Paint paint = new Paint();
        paint.setColor(new Color(HmsResourcesManager.getColorValueByResources(getContext(), ResourceTable.Color_purple_progress)));
        paint.setStyle(Paint.Style.FILL_STYLE);
        return paint;
    }

    /**
     * 设置绘制loading中文字的画笔属性
     *
     * @return 画笔
     */
    protected Paint getTextPaint() {
        Paint paint = new Paint();
        paint.setColor(new Color(
                HmsResourcesManager.getColorValueByResources(getContext(), ResourceTable.Color_white_normal)));
        paint.setStyle(Paint.Style.FILL_STYLE);
        paint.setTextSize(getTextSize());
        paint.setTextAlign(TextAlignment.CENTER);
        return paint;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (mProgress > mMinProgress && mProgress < mMaxProgress) {
            drawProgress(canvas);
        }
    }

    private ShapeElement getRectCompleteShapeElement() {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setCornerRadius(
                HmsResourcesManager.getFloatValueByResources(getContext(), ResourceTable.Float_corner_radius));
        shapeElement.setRgbColor(RgbColor.fromArgbInt(
                HmsResourcesManager.getColorValueByResources(getContext(), ResourceTable.Color_purple_progress)));
        return shapeElement;
    }

    public Paint getmProgressPaint() {
        return mProgressPaint;
    }

    private ShapeElement getRectErrorShapeElement() {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setCornerRadius(
                HmsResourcesManager.getFloatValueByResources(getContext(), ResourceTable.Float_corner_radius));
        shapeElement.setRgbColor(RgbColor.fromArgbInt(
                HmsResourcesManager.getColorValueByResources(getContext(), ResourceTable.Color_red_error)));
        return shapeElement;
    }

    private ShapeElement getRectProgressShapeElement() {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setCornerRadius(
                HmsResourcesManager.getFloatValueByResources(getContext(), ResourceTable.Float_corner_radius));
        shapeElement.setRgbColor(RgbColor.fromArgbInt(
                HmsResourcesManager.getColorValueByResources(getContext(), ResourceTable.Color_purple_progress)));

        return shapeElement;
    }

    private void initAttributes(Context context, AttrSet attributeSet) {
        if (attributeSet == null) {
            return;
        }

        mLoadingText = AttrUtils.getStringValueByAttr(attributeSet,"pb_textProgress", "");
        mCompleteText = AttrUtils.getStringValueByAttr(attributeSet,"pb_textComplete", "");
        mErrorText = AttrUtils.getStringValueByAttr(attributeSet,"pb_textError", "");

        int purple = getColor(ResourceTable.Color_blue_pressed);
        int colorProgress = AttrUtils.getColorValueByAttr(attributeSet,"pb_colorProgress", new Color(purple)).getValue();
        mProgressDrawable.setRgbColor(RgbColor.fromArgbInt(colorProgress));

        int green = getColor(ResourceTable.Color_green_complete);
        int colorComplete = AttrUtils.getColorValueByAttr(attributeSet,"pb_colorComplete", new Color(green)).getValue();
        mCompleteDrawable.setRgbColor(RgbColor.fromArgbInt(colorComplete));
        int red = getColor(ResourceTable.Color_red_error);
        int colorError = AttrUtils.getColorValueByAttr(attributeSet,"pb_colorError", new Color(red)).getValue();
        mErrorDrawable.setRgbColor(RgbColor.fromArgbInt(colorError));
    }

    public void setProgress(int progress) {
        mProgress = progress;
        if (mProgress == mMinProgress) {
            onNormalState();
        } else if (mProgress == mMaxProgress) {
            onCompleteState();
        } else if (mProgress < mMinProgress) {
            onErrorState();
        } else {
            onProgress();
        }

        invalidate();
    }

    private void onErrorState() {
        if (!TextTool.isNullOrEmpty(getErrorText())) {
            setText(getErrorText());
        }
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.RED.getValue()));
        setBackgroundCompat(shapeElement);
    }

    /**
     * 状态变成加载中时背景和文字的切换
     */
    private void onProgress() {
        String loadingText = getLoadingText();
        if (!TextTool.isNullOrEmpty(loadingText)) {
            setText(loadingText);
        }
        setBackgroundCompat(getProgressDrawable());
    }

    private void onCompleteState() {
        if (!TextTool.isNullOrEmpty(getCompleteText())) {
            setText(getCompleteText());
        }
        setBackgroundCompat(getCompleteDrawable());
    }

    private void onNormalState() {
        if (!TextTool.isNullOrEmpty(getNormalText())) {
            setText(getNormalText());
        }
        setBackgroundCompat(getNormalDrawable());
    }

    public abstract void drawProgress(Canvas canvas);

    public int getProgress() {
        return mProgress;
    }

    public void setMaxProgress(int maxProgress) {
        this.mMaxProgress = maxProgress;
    }

    public int getMaxProgress() {
        return mMaxProgress;
    }

    public void setMinProgress(int minProgress) {
        this.mMinProgress = minProgress;
    }

    public int getMinProgress() {
        return mMinProgress;
    }

    public ShapeElement getProgressDrawable() {
        return mProgressDrawable;
    }

    public ShapeElement getCompleteDrawable() {
        return mCompleteDrawable;
    }

    public String getLoadingText() {
        return mLoadingText.toString();
    }

    public String getCompleteText() {
        return mCompleteText.toString();
    }

    public void setProgressDrawable(ShapeElement progressDrawable) {
        mProgressDrawable = progressDrawable;
    }

    public void setCompleteDrawable(ShapeElement completeDrawable) {
        mCompleteDrawable = completeDrawable;
    }

    @Override
    public void setNormalText(CharSequence normalText) {
        super.setNormalText(normalText);
        if (mProgress == mMinProgress) {
            setText(normalText.toString());
        }
    }

    public void setLoadingText(CharSequence loadingText) {
        mLoadingText = loadingText;
    }

    public void setCompleteText(CharSequence completeText) {
        mCompleteText = completeText;
    }

    public ShapeElement getErrorDrawable() {
        return mErrorDrawable;
    }

    public void setErrorDrawable(ShapeElement errorDrawable) {
        mErrorDrawable = errorDrawable;
    }

    public String getErrorText() {
        return mErrorText.toString();
    }

    public void setErrorText(CharSequence errorText) {
        mErrorText = errorText;
    }
}
