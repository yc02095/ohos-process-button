# ohos-process-button


#### 项目介绍
- 项目名称：ohos-process-button
- 所属系列：openharmony的第三方组件适配移植
- 功能：显示Button各种加载状态
- 项目移植状态：100%
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release v0.0.7

#### 效果演示
![输入图片说明](https://gitee.com/chinasoft_ohos/ohos-process-button/raw/master/img/demo.gif "demo.gif")

#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```

2.在entry模块的build.gradle文件中，
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:ohos-process-button:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1.使用相关的类：
- ProcessButton - 可拓展的基础类，实现drawProgress抽象方法可实现自己想要的Button效果
- ActionProcessButton - 用来显示正在loading状态的Button
- GenerateProcessButton - 用来显示当前进度的Button（水平方向）
- SubmitProcessButton - 用来表示当前进度的Button（垂直方向）


2.使用实例
初始sample的构建
```
    public class StateSampleSlice extends Ability implements Component.ClickedListener {
        private ActionProcessButton actionProcessButton;
        private GenerateProcessButton generateProcessButton;
        private SubmitProcessButton submitProcessButton;

        @Override
        protected void onStart(Intent intent) {
            super.onStart(intent);
            super.setUIContent(ResourceTable.Layout_state_sample_silice);
            actionProcessButton = (ActionProcessButton) findComponentById(ResourceTable.Id_state_sample_action);
            actionProcessButton.setMode(ActionProcessButton.Mode.ENDLESS);
            generateProcessButton = (GenerateProcessButton) findComponentById(ResourceTable.Id_state_sample_generate);
            submitProcessButton = (SubmitProcessButton) findComponentById(ResourceTable.Id_state_sample_submit);

            findComponentById(ResourceTable.Id_error_button).setClickedListener(this);
            findComponentById(ResourceTable.Id_noramal_button).setClickedListener(this);
            findComponentById(ResourceTable.Id_half_button).setClickedListener(this);
            findComponentById(ResourceTable.Id_success_button).setClickedListener(this);
        }

        @Override
        public void onClick(Component component) {
            switch (component.getId()) {
                case ResourceTable.Id_error_button:
                    actionProcessButton.setProgress(-1);
                    generateProcessButton.setProgress(-1);
                    submitProcessButton.setProgress(-1);
                    break;
                case ResourceTable.Id_noramal_button:
                    actionProcessButton.setProgress(0);
                    generateProcessButton.setProgress(0);
                    submitProcessButton.setProgress(0);
                    break;
                case ResourceTable.Id_half_button:
                    actionProcessButton.setProgress(50);
                    generateProcessButton.setProgress(50);
                    submitProcessButton.setProgress(50);
                    break;
                case ResourceTable.Id_success_button:
                    actionProcessButton.setProgress(100);
                    generateProcessButton.setProgress(100);
                    submitProcessButton.setProgress(100);
                    break;
            }
        }

        @Override
        protected void onBackground() {
            super.onBackground();
            actionProcessButton.setProgress(0);
            generateProcessButton.setProgress(0);
            submitProcessButton.setProgress(0);
        }

    }
```

3：相关属性的使用示例
```
    <com.dd.processbutton.iml.ActionProcessButton
            ohos:id="$+id:btnSignIn"
            ohos:height="48vp"
            ohos:width="match_parent"
            app:pb_colorComplete="$color:green_complete"
            app:pb_colorNormal="$color:blue_normal"
            app:pb_colorPressed="$color:blue_pressed"
            app:pb_colorProgress="$color:purple_progress"
            app:pb_textComplete="$string:Success"
            app:pb_textError="Error"
            app:pb_textProgress="$string:Loading"
            ohos:text="$string:Sign_in"
            ohos:text_color="#ffffff"
            />

    <com.dd.processbutton.iml.GenerateProcessButton
            ohos:id="$+id:generate_process_btn"
            ohos:height="48vp"
            ohos:width="match_parent"
            ohos:margin="16vp"
            app:pb_textComplete="$string:Done"
            app:pb_textError="Error"
            app:pb_textProgress="$string:Uploading"
            ohos:text="$string:Upload"
            ohos:text_color="#ffffff"
            />


    <com.dd.processbutton.iml.SubmitProcessButton
            ohos:id="$+id:sub_btn_sample"
            ohos:height="48vp"
            ohos:width="100vp"
            app:pb_textComplete="$string:Done"
            app:pb_textError="Error"
            app:pb_textProgress="Loading"
            ohos:text="$string:Send"
            ohos:text_color="#ffffff"
            />

```

4：通用的ProcessButton相关方法的使用说明
```
void setProgress(int progress); 设置按钮进度
int getProgress(); 获取按钮进度

void setMaxProgress(int mMaxProgress); 设置最大按钮进度
int getMaxProgress(); 获取最大按钮进度

void setMinProgress(int mMinProgress); 设置最小按钮进度
int getMinProgress(); 获取最小按钮进度

void setNormalText(CharSequence normalText); 设置正常状态下的Button文本
String getNormalText(); 获取正常状态下的Button文本

void setLoadingText(CharSequence loadingText); 设置loading状态下的Button文本
String getLoadingText(); 获取loading状态下的Button文本

void setCompleteText(CharSequence completeText); 设置完成后的Button文本
String getCompleteText(); 获取完成后的Button文本

void setErrorText(CharSequence errorText); 设置错误状态的Button文本
String getErrorText(); 获取错误状态的Button文本

void setProgressDrawable(ShapeElement progressDrawable); 设置加载中的Button样式
ShapeElement getCompleteDrawable(); 获取加载中的Button样式

void setCompleteDrawable(ShapeElement completeDrawable); 设置完成后的Button样式
ShapeElement getCompleteDrawable(); 获取完成后的Button样式

void setErrorDrawable(ShapeElement errorDrawable); 设置错误状态的Button样式
ShapeElement getErrorDrawable(); 获取错误状态的Button样式

```

5：ActionProcessButton使用示例
```
ActionProcessButton actionProcessButton = (ActionProcessButton) findComponentById(ResourceTable.Id_btnSignIn);
actionProcessButton.setMode(ActionProcessButton.Mode.ENDLESS);设置ActionProcessButton的loading样式为左右闪动
actionProcessButton.setMode(ActionProcessButton.Mode.PROGRESS);设置ActionProcessButton的loading样式为Progress进度样式
actionProcessButton.setColorScheme(int color1, int color2, int color3, int color4);设置ActionProcessButton的闪动样式颜色

```

#### 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

- 1.0.0

#### 版权和许可信息
```
The MIT License (MIT)

Copyright (c) 2014 Danylyk Dmytro

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```