package com.dd.sample;

import com.dd.processbutton.iml.GenerateProcessButton;
import com.dd.processbutton.iml.SubmitProcessButton;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;


public class StateSampleAbility extends Ability implements Component.ClickedListener {

    private GenerateProcessButton mBtnGenerate;
    private SubmitProcessButton mBtnSubmit;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ac_states);
        mBtnSubmit = (SubmitProcessButton) findComponentById(ResourceTable.Id_btnSubmit);
        mBtnGenerate = (GenerateProcessButton) findComponentById(ResourceTable.Id_btnGenerate);

        findComponentById(ResourceTable.Id_btnProgressLoading).setClickedListener(this);
        findComponentById(ResourceTable.Id_btnProgressError).setClickedListener(this);
        findComponentById(ResourceTable.Id_btnProgressComplete).setClickedListener(this);
        findComponentById(ResourceTable.Id_btnProgressNormal).setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btnProgressLoading:
                mBtnSubmit.setProgress(50);
                mBtnGenerate.setProgress(50);
                break;
            case ResourceTable.Id_btnProgressError:
                mBtnSubmit.setProgress(-1);
                mBtnGenerate.setProgress(-1);
                break;
            case ResourceTable.Id_btnProgressComplete:
                mBtnSubmit.setProgress(100);
                mBtnGenerate.setProgress(100);
                break;
            case ResourceTable.Id_btnProgressNormal:
                mBtnSubmit.setProgress(0);
                mBtnGenerate.setProgress(0);
                break;
        }
    }
}
