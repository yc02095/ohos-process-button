/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dd.sample.slice;

import com.dd.processbutton.iml.ActionProcessButton;
import com.dd.processbutton.iml.GenerateProcessButton;
import com.dd.processbutton.iml.SubmitProcessButton;
import com.dd.sample.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

/**
 * 各种Button测试集合
 * @since 2021-03-01
 */
public class StateSampleSlice extends Ability implements Component.ClickedListener {
    private ActionProcessButton actionProcessButton;
    private GenerateProcessButton generateProcessButton;
    private SubmitProcessButton submitProcessButton;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_state_sample_silice);
        actionProcessButton = (ActionProcessButton) findComponentById(ResourceTable.Id_state_sample_action);
        actionProcessButton.setMode(ActionProcessButton.Mode.ENDLESS);
        generateProcessButton = (GenerateProcessButton) findComponentById(ResourceTable.Id_state_sample_generate);
        submitProcessButton = (SubmitProcessButton) findComponentById(ResourceTable.Id_state_sample_submit);

        findComponentById(ResourceTable.Id_error_button).setClickedListener(this);
        findComponentById(ResourceTable.Id_noramal_button).setClickedListener(this);
        findComponentById(ResourceTable.Id_half_button).setClickedListener(this);
        findComponentById(ResourceTable.Id_success_button).setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_error_button:
                actionProcessButton.setProgress(-1);
                generateProcessButton.setProgress(-1);
                submitProcessButton.setProgress(-1);
                break;
            case ResourceTable.Id_noramal_button:
                actionProcessButton.setProgress(0);
                generateProcessButton.setProgress(0);
                submitProcessButton.setProgress(0);
                break;
            case ResourceTable.Id_half_button:
                actionProcessButton.setProgress(50);
                generateProcessButton.setProgress(50);
                submitProcessButton.setProgress(50);
                break;
            case ResourceTable.Id_success_button:
                actionProcessButton.setProgress(100);
                generateProcessButton.setProgress(100);
                submitProcessButton.setProgress(100);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onBackground() {
        super.onBackground();
        actionProcessButton.setProgress(0);
        generateProcessButton.setProgress(0);
        submitProcessButton.setProgress(0);
    }
}
