/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dd.sample.slice;

import com.dd.processbutton.iml.SubmitProcessButton;
import com.dd.sample.ResourceTable;
import com.dd.sample.Toast;
import com.dd.sample.utils.ProgressGenerator;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.TextField;

/**
 * 测试SubmitProcessButton
 * @since 2021-03-01
 */
public class SubmitAbilitySlice extends Ability {
    private SubmitProcessButton submitProcessButton;
    private TextField input;
    private ProgressGenerator progressGenerator;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_submint_ablity_slice);
        progressGenerator = new ProgressGenerator(() ->
                Toast.showLong(getContext(),"Loading Complete, button is disabled"));

        input = (TextField)findComponentById(ResourceTable.Id_input);
        submitProcessButton = (SubmitProcessButton) findComponentById(ResourceTable.Id_sub_btn_sample);
        submitProcessButton.setClickedListener(component -> {
            progressGenerator.start(submitProcessButton);
            submitProcessButton.setEnabled(false);
            input.setEnabled(false);
            input.clearFocus();
        });
    }

    @Override
    protected void onBackground() {
        super.onBackground();
    }

    @Override
    protected void onStop() {
        super.onStop();
        submitProcessButton.setText("SEND");
        submitProcessButton.setProgress(0);
        submitProcessButton.setEnabled(true);
        input.setText("How are you?");
        input.setEnabled(true);
        progressGenerator.stop();
    }
}
