/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dd.sample;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.TextField;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * 自定义状态的TextField
 *
 * @since 2021-03-01
 */
public class StateTextFiled extends TextField {
    private final ShapeElement normalElement = new ShapeElement();
    private final ShapeElement disableElement = new ShapeElement();

    public StateTextFiled(Context context) {
        super(context);
        init();
    }

    public StateTextFiled(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init();
    }

    public StateTextFiled(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    private void init() {
        normalElement.setRgbColor(RgbColor.fromArgbInt(Color.BLACK.getValue()));
        disableElement.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#eeeeee")));
        this.setMultipleLine(false);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (enabled) {
            setBasement(normalElement);
            setTextColor(new Color(Color.getIntColor("#000000")));
        } else {
            setBasement(disableElement);
            setTextColor(new Color(Color.getIntColor("#6C6C6C")));
        }
    }
}
